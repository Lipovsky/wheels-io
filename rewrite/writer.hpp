#pragma once

#include <fallible/result/result.hpp>

#include <wheels/memory/view.hpp>

namespace rewrite {

struct IWriter {
  virtual ~IWriter() = default;

  virtual fallible::Status Write(wheels::ConstMemView data) = 0;
  virtual fallible::Status Flush() = 0;
};

}  // namespace rewrite
