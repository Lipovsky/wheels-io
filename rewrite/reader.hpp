#pragma once

#include <fallible/result/result.hpp>

#include <wheels/memory/view.hpp>

namespace rewrite {

//////////////////////////////////////////////////////////////////////

struct IReader {
  virtual ~IReader() = default;

  // Returns number of bytes read
  // 0 – end of stream
  virtual fallible::Result<size_t> ReadSome(wheels::MutableMemView buffer) = 0;
};

}  // namespace rewrite
