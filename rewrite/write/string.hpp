#pragma once

#include <rewrite/reader.hpp>
#include <rewrite/writer.hpp>

#include <fallible/result/make.hpp>

#include <wheels/memory/view_of.hpp>

#include <string>

namespace rewrite {

class StringWriter : public IWriter {
 public:
  explicit StringWriter(std::string& str) : dest_(str) {
  }

  void Append(wheels::ConstMemView data) {
    dest_.append(data.Begin(), data.Size());
  }

  // IWriter

  fallible::Status Write(wheels::ConstMemView data) override {
    Append(data);
    return fallible::Ok();
  }

  fallible::Status Flush() override {
    return fallible::Ok();
  }

 private:
  std::string& dest_;
};

}  // namespace wheels
