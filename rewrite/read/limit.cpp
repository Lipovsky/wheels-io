#include <rewrite/read/limit.hpp>

#include <fallible/result/make.hpp>

#include <algorithm>

namespace rewrite {

LimitReader::LimitReader(IReader& from, size_t limit)
    : from_(from), left_(limit) {
}

fallible::Result<size_t> LimitReader::ReadSome(wheels::MutableMemView buffer) {
  size_t bytes_to_read = std::min(left_, buffer.Size());
  if (bytes_to_read == 0) {
    return fallible::Ok<size_t>(0);
  }
  auto bytes_read = from_.ReadSome({buffer.Data(), bytes_to_read});
  if (!bytes_read.IsOk()) {
    return fallible::PropagateError(bytes_read);
  }
  left_ -= *bytes_read;
  return fallible::Ok(*bytes_read);
}

}  // namespace rewrite
