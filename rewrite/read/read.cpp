#include <rewrite/read/read.hpp>

#include <rewrite/write/string.hpp>

#include <fallible/result/make.hpp>

#include <wheels/memory/view_of.hpp>

using namespace fallible;

namespace rewrite {

fallible::Result<size_t> Read(IReader& from, wheels::MutableMemView buffer) {
  size_t total_bytes_read = 0;

  while (buffer.HasSpace()) {
    const auto bytes_read = from.ReadSome(buffer);
    if (!bytes_read.IsOk()) {
      return PropagateError(bytes_read);
    }
    if (*bytes_read == 0) {
      break;
    }
    buffer += *bytes_read;
    total_bytes_read += *bytes_read;
  }

  return Ok(total_bytes_read);
}

fallible::Result<size_t> CopyAll(IReader& from, IWriter& to, wheels::MutableMemView buffer) {
  size_t total = 0;
  while (true) {
    auto bytes_read = from.ReadSome(buffer);
    if (!bytes_read.IsOk()) {
      return PropagateError(bytes_read);
    }
    if (*bytes_read == 0) {
      break;
    }
    total += *bytes_read;

    auto written = to.Write({buffer.Begin(), *bytes_read});
    if (!written.IsOk()) {
      return PropagateError(written);
    }
  }
  return Ok(total);
}

fallible::Result<std::string> ReadAll(IReader& from) {
  std::string all;
  StringWriter all_writer(all);

  char buffer[1024];

  auto copied = CopyAll(from, /*to=*/all_writer, wheels::MutViewOf(buffer));
  if (!copied.IsOk()) {
    return PropagateError(copied);
  }
  return Ok(all);
}

}  // namespace rewrite
