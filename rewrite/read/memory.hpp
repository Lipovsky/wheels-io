#pragma once

#include <rewrite/reader.hpp>

namespace rewrite {

class MemoryReader : public IReader {
 public:
  MemoryReader(wheels::ConstMemView source);

  // Empty
  MemoryReader();

  fallible::Result<size_t> ReadSome(wheels::MutableMemView buffer) override;

  // Always succeeded
  size_t Read(wheels::MutableMemView buffer);

  // Reset memory source

  void Reset(wheels::ConstMemView source) {
    source_ = source;
  }

  void Reset() {
    Reset(wheels::ConstMemView::Empty());
  }

  bool Exhausted() const noexcept {
    return source_.IsEmpty();
  }

  size_t BytesLeft() const noexcept {
    return source_.Size();
  }

 private:
  wheels::ConstMemView source_;
};

}  // namespace rewrite
