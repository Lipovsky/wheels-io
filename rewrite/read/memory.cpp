#include <rewrite/read/memory.hpp>

#include <fallible/result/make.hpp>

#include <algorithm>
#include <cstring>

namespace rewrite {

MemoryReader::MemoryReader(wheels::ConstMemView source)
    : source_(source) {
}

MemoryReader::MemoryReader()
    : MemoryReader({nullptr, 0}) {
}

size_t MemoryReader::Read(wheels::MutableMemView buffer) {
  size_t to_copy = std::min(buffer.Size(), source_.Size());
  if (to_copy > 0) {
    memcpy(buffer.Data(), source_.Data(), to_copy);
    source_ += to_copy;
  }
  return to_copy;
}

fallible::Result<size_t> MemoryReader::ReadSome(wheels::MutableMemView buffer) {
  return fallible::Ok(Read(buffer));
}

}  // namespace rewrite
