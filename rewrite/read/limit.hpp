#pragma once

#include <rewrite/reader.hpp>

namespace rewrite {

class LimitReader : public IReader {
 public:
  LimitReader(IReader& from, size_t limit);

  fallible::Result<size_t> ReadSome(wheels::MutableMemView buffer) override;

 private:
  IReader& from_;
  size_t left_;
};

}  // namespace rewrite
