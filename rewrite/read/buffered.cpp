#include <rewrite/read/buffered.hpp>

#include <fallible/result/make.hpp>

#include <wheels/memory/view_of.hpp>

namespace rewrite {

BufferedReader::BufferedReader(IReader& from, size_t buffer_size)
    : from_(from), buffer_(buffer_size) {
  buffer_reader_.Reset();
}

fallible::Result<size_t> BufferedReader::ReadSome(wheels::MutableMemView dest) {
  if (!buffer_reader_.Exhausted()) {
    return fallible::Ok(buffer_reader_.Read(dest));
  }

  auto refilled = ReFillBuffer();
  if (!refilled.IsOk()) {
    return fallible::PropagateError(refilled);
  }

  if (!buffer_reader_.Exhausted()) {
    return fallible::Ok(buffer_reader_.Read(dest));
  } else {
    return fallible::Ok<size_t>(0);
  }
}

fallible::Status BufferedReader::ReFillBuffer() {
  auto bytes_read = from_.ReadSome(wheels::MutViewOf(buffer_));
  if (!bytes_read.IsOk()) {
    return fallible::PropagateError(bytes_read);
  }
  if (*bytes_read > 0) {
    buffer_reader_.Reset({buffer_.data(), *bytes_read});
  } else {
    buffer_reader_.Reset();
  }
  return fallible::Ok();
}

}  // namespace rewrite
