#pragma once

#include <rewrite/reader.hpp>
#include <rewrite/read/memory.hpp>

namespace rewrite {

class BufferedReader : public IReader {
  static const size_t kDefaultBufferSize = 1024;

 public:
  BufferedReader(IReader& from, size_t buffer_size = kDefaultBufferSize);

  fallible::Result<size_t> ReadSome(wheels::MutableMemView dest) override;

 private:
  fallible::Status ReFillBuffer();

 private:
  IReader& from_;
  std::vector<char> buffer_;
  MemoryReader buffer_reader_;
};

}  // namespace rewrite
