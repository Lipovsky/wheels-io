#pragma once

#include <rewrite/reader.hpp>
#include <rewrite/writer.hpp>

#include <string>

namespace rewrite {

fallible::Result<size_t> Read(IReader& from, wheels::MutableMemView buffer);

fallible::Result<size_t> CopyAll(IReader& from, IWriter& to, wheels::MutableMemView buffer);

fallible::Result<std::string> ReadAll(IReader& from);

}  // namespace rewrite
